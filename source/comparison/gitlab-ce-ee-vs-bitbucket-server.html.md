---
layout: comparison_page
trial_bar: true
title: GitLab compared to other tools
suppress_header: true
image_title: '/images/comparison/title_image.png'
extra_css:
  - compared.css
---

## GitLab CE/EE vs. BitBucket Server

### Built-in Continuous Integration tool
Easy-to-set up, performant, stable and elegant continuous integration with every GitLab installation.

[Learn about the benefits of our CI tool](https://docs.gitlab.com/ce/ci/)

### More value with everything you need in one solution
To replace GitLab, you'd need a combination of Bitbucket server, JIRA, Confluence, and Bamboo.

### Great user interface
GitLab has a really nice user interface that your team will love, and also enjoy working with.

### Integrated issue tracking
GitLab includes an issue tracker which you can link to any merge request.

[Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues)

### Better activity feed and visualization
Collaborative activity feed to help everyone understand what is happening in a project and graphical view of the commit history.

[See Our Activity Feed](https://gitlab.com/gitlab-org/gitlab-ce/activity)

### Full, powerful search
Search through all your code, issues, wikis, commits, groups and projects. Built into GitLab.

### No need to manage licenses
GitLab EE needs a license, but doesn't require any validation or separate
licenses for separate servers. With BitBucket Server (Data Center), licenses
are linked to servers, requiring management of licenses over servers.
You can deploy 100 instances of GitLab EE for the same price as a single
instance. You only pay for the total amount of users, not for how you choose to
manage and scale the software.

### Easy configuration
GitLab can be easily configured through ENV variables and YAML files.
This makes maintaining, upgrading and deploying GitLab easy to automate.
BitBucket Server requires extensive manual configuration.
